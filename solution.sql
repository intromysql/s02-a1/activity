CREATE DATABASE blog_db

USE blog_db;

create table users (
  id int auto_increment not null primary key,
  email varchar(100) not null,
  password varchar(300) not null,
  datetime_created datetime not null
);



create table posts (
  id int auto_increment not null primary key,
  author_id int not null,
  title varchar(500) not null,
  content varchar(5000) not null,
  datetime_posted datetime not null,
  CONSTRAINT fk_posts_author_id
    FOREIGN KEY (author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
  );


create table post_likes (
  id int auto_increment not null primary key,
  post_id int not null,
  user_id int not null,
  datetime_liked datetime not null,
 
 CONSTRAINT fk_posts_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

CONSTRAINT fk_posts_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT

);


create table post_comments (
  id int auto_increment not null primary key,
  post_id int not null,
  user_id int not null,
  content varchar(5000) not null,
  datetime_commented datetime not null,
  
CONSTRAINT fk_post_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,


CONSTRAINT fk_post_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT

);




--data test
insert into users (id, email,password,datetime_created) values (1,'alan@gmail.com','Pa$$word',now());
insert into users (id, email,password,datetime_created) values (2,'ben@gmail.com','Pa$$word',now());




