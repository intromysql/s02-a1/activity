-- List down all the databases inside the DBMS

SHOW DATABASES;

-- CREATE a database.
-- Syntax:
	-- CREATE DATABASE <name_db>

CREATE DATABASE music_db;

-- DROP DATABASE, deletion of database
-- Syntax:
	-- DROP DATABASE <name_db>

DROP DATABASE music_db;

-- SELECT a database.
-- Syntax
	-- USE <name_db>
USE music_db;

-- CREATE tables.
-- Table columns will also be declared here
CREATE TABLE users(
	-- INT to indicate that the id column will have integer entry
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number CHAR(11) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),
	-- To set the primary key
	PRIMARY KEY (id)
);

-- Drop table from database

DROP TABLE userss;

CREATE TABLE playlists (
	id INT NOT NULL  AUTO_INCREMENT,
	user_id INT NOT NULL,
	playlist_name VARCHAR(30) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- artists table
CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);


-- Mini-activity for the albums
-- columns: id, album_title, date_released, artist_id
	-- CASCADE
	-- Cannot delete the artist
	-- declare yung foreign and PRIMARY key



create table albums (
  id int auto_increment not null primary key,
  artist_id int not null,
  album_id int not null,
  album_title varchar(100) not null,
  datetime_release datetime not null,
  CONSTRAINT fk_albums_artist_id
    FOREIGN KEY (artist_id) REFERENCES artists(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);



-- Mini-activity for the albums
-- columns: id, song_name, lenght, genre, album_id
	-- CASCADE
	-- Cannot delete the artist
	-- declare yung foreign and PRIMARY key

create table songs (
  id int auto_increment not null primary key,
  song_name varchar(25) not null,
  lenght TIME not null,
  genre varchar(20),
  album_id int not null,
  CONSTRAINT fk_songs_album_id
    FOREIGN KEY (album_id) REFERENCES albums(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);



-- Mini-activity for the playlist songs
-- columns: id, playlist_id, song_id

create table playlists_songs (
  id meduimint auto_increment not null primary key,
  playlists_id int not null,
  song_id int not null,
  CONSTRAINT fk_playlists_songs_id
    FOREIGN KEY (playlists_id) REFERENCES playlists(id),
    FOREIGN KEY (song_id) REFERENCES songs(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT
);



-- Mini activity

-- CREATE a table for the playlists_songs
-- id, playlist_id, song_id

CREATE TABLE playlist_songs (
	id MEDIUMINT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES
			songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);
